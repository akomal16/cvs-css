import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ResearchComponent} from './research-tags/research.component';
import {HomeComponent} from './home/home.component';
import {ReportComponent} from './report/report.component';
import {AddComponent} from './add/add.component';
import {UserComponent} from './user/user.component';
import {DrugsComponent} from './drugs/drugs.component';
import {SearchComponent} from './search/search.component';
import {EmployeeComponent} from './employee/employee.component';
// import {TableComponent} from './table/table.component';

const routes: Routes = [
  {path: 'research', component: ResearchComponent},
  {path: 'home', component: HomeComponent},
  {path: 'report', component: ReportComponent},
  {path: 'search', component: SearchComponent},
  {path: 'add', component: AddComponent},
  {path: 'employee', component: EmployeeComponent},
  {path: 'user', component: UserComponent},
  {path: 'drugs', component: DrugsComponent},
  {path: '**', redirectTo: 'home', pathMatch: 'full'}
  // {path: 'table', component: TableComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
