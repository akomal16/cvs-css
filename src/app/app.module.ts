import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatCardModule, MatCheckboxModule, MatInputModule, MatRadioModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ResearchComponent} from './research-tags/research.component';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './home/home.component';
import {ReportComponent} from './report/report.component';
import {AddComponent} from './add/add.component';
import {UserComponent} from './user/user.component';
import {DrugsComponent} from './drugs/drugs.component';
import {SearchComponent} from './search/search.component';
import {EmployeeComponent} from './employee/employee.component';


@NgModule({
  declarations: [
    AppComponent,
    ResearchComponent,
    HomeComponent,
    ReportComponent,
    EmployeeComponent,
    UserComponent,
    SearchComponent,
    AddComponent,
    DrugsComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    AppRoutingModule,
    MatRadioModule,
    HttpClientModule,
    MatCheckboxModule,
    MatInputModule,
    FormsModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
