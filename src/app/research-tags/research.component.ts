import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.scss']
})
export class ResearchComponent implements OnInit {


  @Input() parentForm: FormGroup;
  @Input() amarForm: FormGroup;
  @Input() phoneValue = '(Optional)';

  business = [
    {value: 'Commercial'},
    {value: 'MMC'},
    {value: 'DSC'},
  ];

  drugs = [
    {value: 'Concerta'},
    {value: 'Myprodol'},
    {value: 'Derex'}
  ];

  quantities = [
    {value: 'EQ'},
    {value: 'OK'},
    {value: 'YES'},
  ];

  cvs = [
    {value: 'CVS Caremark'},
    {value: 'CVS HealthCare'},
  ]

  amar = [
    {value: 'ABV_PBM_REB_1011.19'},
    {value: 'OABV_PBM_REB_1021.29'},
    {value: 'ABV_PBM_REB_1031.39'},
  ];

  toggled = false;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.parentForm = new FormGroup({
      'business': new FormControl('Commercial'),
      'drugs': new FormControl('Myprodol'),
      'auth': new FormControl('EQ'),
      'quantities': new FormControl('OK'),
      'id': new FormControl('Amar'),
      'cvs': new FormControl('CVS Caremark')

    });
    //
    // this.amarForm = new FormGroup({
    //   'cvs': new FormControl('CVS Caremark')
    // });
  }
}
